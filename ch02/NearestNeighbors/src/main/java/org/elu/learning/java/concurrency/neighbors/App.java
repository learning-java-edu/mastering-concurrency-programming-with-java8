package org.elu.learning.java.concurrency.neighbors;

import org.elu.learning.java.concurrency.neighbors.data.BankMarketing;
import org.elu.learning.java.concurrency.neighbors.loader.BankMarketingLoader;
import org.elu.learning.java.concurrency.neighbors.main.Classifier;
import org.elu.learning.java.concurrency.neighbors.parallel.group.KnnClassifierParallelGroup;
import org.elu.learning.java.concurrency.neighbors.parallel.individual.KnnClassifierParallelIndividual;
import org.elu.learning.java.concurrency.neighbors.serial.KnnClassifier;

import java.util.Date;
import java.util.List;

public class App {
    private enum RunType {
        SERIAL("serial"),
        PARALLEL_INDIVIDUAL("individual"),
        PARALLEL_INDIVIDUAL_SORT("individual_sort"),
        PARALLEL_GROUP("group"),
        PARALLEL_GROUP_SORT("group_sort");

        private String title;

        RunType(String title) {
            this.title = title;
        }

        public static RunType fromTitle(String string) {
            for (RunType type : RunType.values()) {
                if (type.title.equalsIgnoreCase(string)) {
                    return type;
                }
            }
            return SERIAL;
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: script <k> <type>");
            System.exit(1);
        }
        BankMarketingLoader loader = new BankMarketingLoader();
        List<BankMarketing> train = loader.load("data/bank.data");
        System.out.println("Train: " + train.size());
        List<BankMarketing> test = loader.load("data/bank.test");
        System.out.println("Test: " + test.size());
        double currentTime = 0d;
        int success = 0, mistakes = 0;
        int k = Integer.parseInt(args[0]);
        RunType type = RunType.fromTitle(args[1]);
        Classifier classifier;

        switch (type) {
            default:
            case SERIAL:
                classifier = new KnnClassifier(train, k);
                break;
            case PARALLEL_INDIVIDUAL:
                classifier = new KnnClassifierParallelIndividual(train, k, 1, false);
                break;
            case PARALLEL_INDIVIDUAL_SORT:
                classifier = new KnnClassifierParallelIndividual(train, k, 1, true);
                break;
            case PARALLEL_GROUP:
                classifier = new KnnClassifierParallelGroup(train, k, 1, false);
                break;
            case PARALLEL_GROUP_SORT:
                classifier = new KnnClassifierParallelGroup(train, k, 1, true);
                break;
        }

        try {
            Date start, end;
            start = new Date();
            for (BankMarketing example : test) {
                String tag = classifier.classify(example);
                if (tag.equals(example.getTag())) {
                    success++;
                } else {
                    mistakes++;
                }
            }
            end = new Date();

            currentTime = end.getTime() - start.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        classifier.destroy();
        System.out.println("******************************************");
        printTitle(type, k);
        System.out.println("Success: " + success);
        System.out.println("Mistakes: " + mistakes);
        System.out.println("Execution Time: " + (currentTime / 1000) + " seconds.");
        System.out.println("******************************************");
    }

    private static void printTitle(RunType type, int k) {
        switch (type) {
            default:
            case SERIAL:
                System.out.println("Serial Classifier - K: " + k);
                break;
            case PARALLEL_INDIVIDUAL:
                System.out.println("Parallel Classifier Individual - K: " + k + " - Factor 1 - Parallel Sort: false");
                break;
            case PARALLEL_INDIVIDUAL_SORT:
                System.out.println("Parallel Classifier Individual - K: " + k + " - Factor 1 - Parallel Sort: true");
                break;
            case PARALLEL_GROUP:
                System.out.println("Parallel Classifier Group - K: " + k + " - Factor 1 - Parallel Sort: false");
                break;
            case PARALLEL_GROUP_SORT:
                System.out.println("Parallel Classifier Group - K: " + k + " - Factor 1 - Parallel Sort: true");
                break;
        }
    }
}
