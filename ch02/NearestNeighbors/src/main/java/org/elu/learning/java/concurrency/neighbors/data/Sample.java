package org.elu.learning.java.concurrency.neighbors.data;

public abstract class Sample {
    public abstract String getTag();
    public abstract double[] getExample();
}
