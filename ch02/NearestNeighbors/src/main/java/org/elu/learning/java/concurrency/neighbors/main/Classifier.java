package org.elu.learning.java.concurrency.neighbors.main;

import org.elu.learning.java.concurrency.neighbors.data.Sample;

public interface Classifier {
    String classify (Sample example) throws Exception;

    default void destroy() {}
}
